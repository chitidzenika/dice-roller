package com.example.loginaplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth

class ForgotActivity : AppCompatActivity() {
    private lateinit var editTextForgotEmailAddress: EditText
    private lateinit var buttonForgot: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot)
        init()
        registerListener()


    }

    private fun init(){
        editTextForgotEmailAddress = findViewById(R.id.editTextForgotEmailAddress)
        buttonForgot = findViewById(R.id.buttonForgot)
    }

    private fun registerListener(){

        buttonForgot.setOnClickListener {
            val email = editTextForgotEmailAddress.text.toString()

            if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(this, "Incorrect Email", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            AlertDialog.Builder(this).setTitle("Confirm").setMessage("Are you sure you want to send recovery on this E-mail?")
                .setPositiveButton("Yes"){dialog, i ->

                    FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener { task ->
                        if (task.isSuccessful){
                            Toast.makeText(this, "Check Your Inbox", Toast.LENGTH_SHORT).show()
                            startActivity(Intent(this,LoginActivity::class.java))
                            finish()
                            dialog.dismiss()
                        }else{
                            Toast.makeText(this, "This E-mail Doesn't Exist", Toast.LENGTH_SHORT).show()
                            dialog.dismiss()

                        }
                    }

                }.setNegativeButton("No"){dialog, i ->

                    dialog.dismiss()

                }.show().setCancelable(false)


        }

    }

}