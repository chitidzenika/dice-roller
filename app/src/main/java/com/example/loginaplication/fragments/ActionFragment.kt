package com.example.loginaplication.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.example.loginaplication.R
import kotlin.random.Random


class ActionFragment : Fragment() {

    private lateinit var dice1: ImageView
    private lateinit var dice2: ImageView
    private lateinit var rollButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_action, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dice1 = view.findViewById(R.id.imageViewDice1)
        dice2 = view.findViewById(R.id.imageViewDice2)
        rollButton = view.findViewById(R.id.buttonRoll)


        rollButton.setOnClickListener {


            random()
            random2()


        }
    }


    private fun random(){

        val randInt = java.util.Random().nextInt(6) + 1
        val draw = when(randInt){
            1 -> R.drawable.dice1
            2 -> R.drawable.dice2
            3 -> R.drawable.dice3
            4 -> R.drawable.dice4
            5 -> R.drawable.dice5
            else -> R.drawable.dice6

        }
        dice1.setImageResource(draw)

    }

    private fun random2(){

        val randInt = java.util.Random().nextInt(6) + 1
        val draw = when(randInt){
            1 -> R.drawable.dice1
            2 -> R.drawable.dice2
            3 -> R.drawable.dice3
            4 -> R.drawable.dice4
            5 -> R.drawable.dice5
            else -> R.drawable.dice6

        }
        dice2.setImageResource(draw)

    }


}