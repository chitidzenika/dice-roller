package com.example.loginaplication.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.loginaplication.LoginActivity
import com.example.loginaplication.PasswordChangeActivity
import com.example.loginaplication.R
import com.example.loginaplication.UserInfo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ProfileFragment : Fragment() {

    private lateinit var imageView: ImageView
    private lateinit var textViewUsername: TextView
    private lateinit var textViewAge: TextView
    private lateinit var textViewCity: TextView
    private lateinit var editTextUrl: EditText
    private lateinit var editTextUsername: EditText
    private lateinit var editTextAge: EditText
    private lateinit var editTextCity: EditText
    private lateinit var buttonChange: Button
    private lateinit var buttonLogout: Button
    private lateinit var buttonSave: Button

    val auth = FirebaseAuth.getInstance()

    val db = FirebaseDatabase.getInstance().getReference("UserInfo")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        imageView = view.findViewById(R.id.imageView)
        textViewUsername = view.findViewById(R.id.textViewUsername)
        editTextUsername = view.findViewById(R.id.editTextUsername)
        textViewAge = view.findViewById(R.id.textViewAge)
        textViewCity = view.findViewById(R.id.textViewCity)
        editTextUrl = view.findViewById(R.id.editTextUrl)
        editTextCity = view.findViewById(R.id.editTextCity)
        editTextAge = view.findViewById(R.id.editTextAge)
        buttonChange = view.findViewById(R.id.buttonChange)
        buttonLogout = view.findViewById(R.id.buttonLogout)
        buttonSave = view.findViewById(R.id.buttonSave)



        buttonSave.setOnClickListener {

            val ageText = editTextAge.text.toString()
            val userText = editTextUsername.text.toString()
            val cityText = editTextCity.text.toString()
            val url = editTextUrl.text.toString()
            val userInfo = UserInfo(userText, ageText, cityText, url)

            if (userInfo.age.isEmpty() || userInfo.city.isEmpty() || userInfo.url.isEmpty() || userInfo.username.isEmpty()){
            }else{
                db.child(auth.currentUser?.uid!!).setValue(userInfo)
            }


        }



        db.child(auth.currentUser?.uid!!).addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                val userInfo = snapshot.getValue(UserInfo::class.java) ?: return

                Glide.with(this@ProfileFragment).load(userInfo.url).into(imageView)


                textViewUsername.text = "Name: " + userInfo.username
                textViewAge.text = "Age: " + userInfo.age
                textViewCity.text = "City: " + userInfo.city

            }

            override fun onCancelled(error: DatabaseError) {

            }

        })



        buttonLogout.setOnClickListener {


            FirebaseAuth.getInstance().signOut()

            requireActivity().run {
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            }

        }

        buttonChange.setOnClickListener {

            requireActivity().run {
                startActivity(Intent(this, PasswordChangeActivity::class.java))
            }

        }


    }


}