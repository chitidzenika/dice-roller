package com.example.loginaplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.ktx.Firebase

class LoginActivity : AppCompatActivity() {

    private lateinit var buttonRegistration: Button
    private lateinit var buttonLogin: Button
    private lateinit var buttonForgot: Button
    private lateinit var editTextEmailAddress: EditText
    private lateinit var editTextPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
        listener()

    }

    private fun init(){
        buttonRegistration = findViewById(R.id.buttonRegistration)
        editTextEmailAddress = findViewById(R.id.editTextEmailAddress)
        editTextPassword = findViewById(R.id.editTextPassword)
        buttonLogin = findViewById(R.id.buttonLogin)
        buttonForgot = findViewById(R.id.buttonForgot)

    }

    private fun listener(){
        buttonRegistration.setOnClickListener {
            val intent = Intent(this,RegisterActivity::class.java)
            startActivity(intent)
        }

        buttonLogin.setOnClickListener {
            val email = editTextEmailAddress.text.toString()
            val password = editTextPassword.text.toString()

            if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                Toast.makeText(this, "Incorrect Email", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.isEmpty() || password.length <6 || password.length > 16) {
                Toast.makeText(this, "Incorrect Password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            FirebaseAuth.getInstance().signInWithEmailAndPassword(email,password).addOnCompleteListener { task ->
                if (task.isSuccessful){
                    startActivity(Intent(this,ProfileActivity::class.java))
                    finish()
                }else{
                    Toast.makeText(this, "Email Or Password is incorrect", Toast.LENGTH_SHORT).show()
                }
            }


        }

        buttonForgot.setOnClickListener {
            startActivity(Intent(this, ForgotActivity::class.java))
        }

    }
}