package com.example.loginaplication

data class UserInfo(
    val username :String = "",
    val age :String = "",
    val city :String = "",
    val url :String = "",
)
