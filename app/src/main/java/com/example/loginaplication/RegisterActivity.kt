package com.example.loginaplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var editTextRegisterEmailAddress: EditText
    private lateinit var editTextRegisterPassword: EditText
    private lateinit var editTextRegisterPassword2: EditText
    private lateinit var buttonRegister: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
        registerListeners()


    }


    private fun init() {
        editTextRegisterEmailAddress = findViewById(R.id.editTextRegisterEmailAddress)
        editTextRegisterPassword = findViewById(R.id.editTextRegisterPassword)
        editTextRegisterPassword2 = findViewById(R.id.editTextRegisterPassword2)
        buttonRegister = findViewById(R.id.buttonRegister)

    }

    private fun registerListeners() {
        buttonRegister.setOnClickListener {
            val email = editTextRegisterEmailAddress.text.toString()
            val password = editTextRegisterPassword.text.toString()
            val repeatPassword = editTextRegisterPassword2.text.toString()

            if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                Toast.makeText(this, "Incorrect Email", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.isEmpty()) {
                Toast.makeText(this, "Fill Password", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.length < 6 || password.length > 16) {
                Toast.makeText(
                    this,
                    "Password Must Be Between 6..16 characters",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }
            if (repeatPassword != password) {
                Toast.makeText(
                    this,
                    "Passwords Doesn't Match",
                    Toast.LENGTH_SHORT
                ).show()
            }



            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this, "Registration Completed", Toast.LENGTH_SHORT).show()
                        startActivity(Intent(this,LoginActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this, "Registration Failed!", Toast.LENGTH_SHORT).show()
                    }
                }


        }


    }

}