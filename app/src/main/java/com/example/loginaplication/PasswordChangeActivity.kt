package com.example.loginaplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.firebase.auth.FirebaseAuth

class PasswordChangeActivity : AppCompatActivity() {


    private lateinit var editTextChangePasswordNew: EditText
    private lateinit var editTextChangePasswordNew2: EditText
    private lateinit var buttonChange: Button

    val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change)
        init()
        registerListener()


    }


    private fun init(){
        
        editTextChangePasswordNew = findViewById(R.id.editTextChangePasswordNew)
        editTextChangePasswordNew2 = findViewById(R.id.editTextChangePasswordNew2)
        buttonChange = findViewById(R.id.buttonChange)

    }



    private fun registerListener(){

        buttonChange.setOnClickListener {
            val newPassword = editTextChangePasswordNew.text.toString()
            val newPassword2 = editTextChangePasswordNew2.text.toString()
            
            
            if (newPassword.isEmpty() || newPassword2.isEmpty()){
                Toast.makeText(this, "Enter Passwords", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (newPassword.length < 6 || newPassword.length > 16) {
                Toast.makeText(
                    this,
                    "Password Must Be Between 6..16 characters",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            if (newPassword != newPassword2){
                Toast.makeText(this, "New Passwords don't match", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            AlertDialog.Builder(this).setTitle("Confirm").setMessage("Are you sure you want to change password?")
                .setPositiveButton("Yes"){dialog, i ->
                    if (newPassword == newPassword2){
                        auth.currentUser?.updatePassword(newPassword)?.addOnCompleteListener { task ->

                            if (task.isSuccessful){
                                Toast.makeText(this, "Password has been changed", Toast.LENGTH_SHORT).show()
                                FirebaseAuth.getInstance().signOut()
                                startActivity(Intent(this,ProfileActivity::class.java))
                                finish()
                                dialog.dismiss()

                            }else{
                                Toast.makeText(this, "error", Toast.LENGTH_SHORT).show()
                                dialog.dismiss()
                            }

                        }
                    }
                }.setNegativeButton("No"){dialog, i ->
                    dialog.dismiss()
                }
                .show().setCancelable(false)



            
            
            





        }

    }

}